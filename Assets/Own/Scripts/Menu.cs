﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject Character;

    float Timer = 5.0f;
    int HighScore = 0;

    // Use this for initialization
    void Start()
    {
        HighScore = PlayerPrefs.GetInt("HighScore");
    }

    // Update is called once per frame
    void Update()
    {
        if (Character && Character.GetComponentInChildren<Combatant>().IsDead)
        {
            if (Timer > 0.0f)
            {
                Timer -= Time.deltaTime;
            }
            else
            {
                transform.Find("Canvas").gameObject.SetActive(true);

                int score = Character.GetComponent<Character>().GetScore();
                if (score > HighScore)
                {
                    HighScore = score;
                    PlayerPrefs.SetInt("HighScore", HighScore);
                }

                GameObject obj = GameObject.Find("ScoreText").gameObject;

                if (obj)
                {
                    obj.GetComponent<Text>().text = "Your Score: " + score + "\nBest Score :" + HighScore;
                }

                if (Input.GetKey(KeyCode.Space))
                {
                    ClickAgain();
                }
            }
        }
    }

    public void ClickAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
