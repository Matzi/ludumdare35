﻿using UnityEngine;
using System.Collections;

public class Creature : MonoBehaviour {
    public GameObject PowerUp;
    public int SpawnWeight = 10;

    Animator animator;
    CharacterController ctrl;
    CreatureManager manager;
    Combatant combatant;
    Vector3 targetPosition;
    bool powerUp = false;
    float rotateSpeed = 20.0f;
    float runSpeed = 6.0f;
    float distanceThreshold = 10.0f;
    float deathTimer = 20.0f;
    float verticalSpeed = 0.0f;

    int CountClose = 0;

    // Use this for initialization
    void Start () {
        animator = GetComponentInChildren<Animator>();
        manager = FindObjectOfType<CreatureManager>();
        ctrl = GetComponentInChildren<CharacterController>();
        combatant = GetComponentInChildren<Combatant>();
        manager.AddCreatrure(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        if (combatant.IsDead)
        {
            animator.SetInteger("Death", 1);
            GetComponentInChildren<CharacterController>().enabled = false;
            manager.RemoveCreatrure(gameObject);

            deathTimer -= Time.deltaTime;
            if (deathTimer < 15.0f && !powerUp && PowerUp)
            {
                powerUp = true;
                GameObject.Instantiate(PowerUp, transform.position, Quaternion.identity);
            }
            if (deathTimer < 0.0f)
            {
                Destroy(gameObject);
            }

            return;
        }

        if (combatant.wasDamaged)
        {
            animator.SetTrigger("Pain");
            combatant.wasDamaged = false;
        }

        if (combatant.CanMove())
        {
            Vector3 dist = (gameObject.transform.position - targetPosition);
            if (targetPosition.sqrMagnitude > 0.1f && CountClose > 2 && dist.sqrMagnitude > distanceThreshold * distanceThreshold)
            {
                targetPosition = targetPosition + dist.normalized * (distanceThreshold + 1.0f);
            }

            if (targetPosition.sqrMagnitude > 0.1f)
            {
                Quaternion tempRot = transform.rotation;
                transform.LookAt(targetPosition);
                Quaternion hitRot = transform.rotation;

                transform.rotation = Quaternion.Slerp(tempRot, hitRot, Time.deltaTime * rotateSpeed);

                if ((transform.position - targetPosition).sqrMagnitude < combatant.attackDistance * combatant.attackDistance)
                {
                    combatant.LaunchAttack();
                    animator.SetTrigger("Use");
                    animator.SetBool("Idling", true);
                }
                else
                {
                    animator.SetBool("Idling", false);
                    ctrl.Move((targetPosition - transform.position).normalized * Time.deltaTime * runSpeed);
                }
            }
            else
            {
                animator.SetBool("Idling", true);
            }
        }

        verticalSpeed += 10.0f * Time.deltaTime;
        ctrl.Move(Vector3.down * (Time.deltaTime * verticalSpeed));
        if (ctrl.isGrounded)
        {
            verticalSpeed = 0.0f;
        }
        targetPosition = Vector3.zero;
    }

    public void Notice(GameObject obj, int countClose)
    {
        Character ch = obj.GetComponent<Character>();
        if (ch)
        {
            CountClose = countClose;
            targetPosition = obj.transform.position;
            targetPosition.y = gameObject.transform.position.y;
        }
    }
}
