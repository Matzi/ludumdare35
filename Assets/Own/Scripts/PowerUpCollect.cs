﻿using UnityEngine;
using System.Collections;

public class PowerUpCollect : MonoBehaviour {
    Character character = null;
    public float time = 10.0f;
    public float deathTime = 0.0f;

    // Use this for initialization
    void Start () {
        character = FindObjectOfType<Character>();
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime;

        if (time < 0.0f)
        {
            End();
        }
	}

    void OnTriggerEnter(Collider other)
    {
        character.PowerUp();
        End();
    }

    void End()
    {
        deathTime = 1.0f;
        GetComponentInChildren<ParticleSystem>().Stop();
        Destroy(this);
        Destroy(gameObject, 2);
    }
}
