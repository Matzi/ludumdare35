﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatureManager : MonoBehaviour
{
    public GameObject[] Enemies;
    public float MaxEnemyCount = 20;
    public float HealthBonusPerSecond = 0.15f;
    public float DamageBonusPerSecond = 0.05f;
    public float weightBonusPerSecond = 0.06f;

    List<GameObject> creatures;
    List<GameObject> spawnPoints;

    Combatant combatant;
    float distanceThreshold = 10.0f;

    float totalTime = 0.0f;

    CreatureManager()
    {
        creatures = new List<GameObject>();
        spawnPoints = new List<GameObject>();
    }

    // Use this for initialization
    void Start()
    {
        combatant = GetComponentInChildren<Combatant>();
    }

    // Update is called once per frame
    void Update()
    {
        if (combatant.IsDead)
        {
            return;
        }

        totalTime += Time.deltaTime;

        int close = 0;
        foreach (GameObject creature in creatures)
        {
            Vector3 dist = (creature.transform.position - gameObject.transform.position);
            dist.y = 0;
            if (!combatant.IsDead && !creature.GetComponentInChildren<Combatant>().IsDead &&
                dist.sqrMagnitude < distanceThreshold * distanceThreshold)
            {
                ++close;
            }
        }
        foreach (GameObject creature in creatures)
        {
            Creature c = creature.GetComponent<Creature>();
            if (!combatant.IsDead && !c.GetComponentInChildren<Combatant>().IsDead)
            {
                c.Notice(gameObject, close);
            }
        }

        if (creatures.Count < MaxEnemyCount && Enemies.Length > 0 && spawnPoints.Count > 0)
        {
            int spawnBias = (int)(totalTime * weightBonusPerSecond);
            int number = Random.Range(0, 100 + spawnBias * Enemies.Length);
            GameObject spawn = spawnPoints[Random.Range(0, spawnPoints.Count)];
            if (spawn.GetComponentInChildren<SpawnPoint>().IsActive)
            {
                int index = -1;

                do
                {
                    index = (index + 1) % Enemies.Length;

                    number -= Enemies[index].GetComponentInChildren<Creature>().SpawnWeight + spawnBias;
                } while (number > 0);

                GameObject obj = (GameObject)Object.Instantiate(Enemies[index], spawn.transform.position, Quaternion.identity);

                obj.GetComponentInChildren<Combatant>().Improve(totalTime * HealthBonusPerSecond, totalTime * DamageBonusPerSecond);
            }
        }
    }

    public void AddCreatrure(GameObject creature)
    {
        creatures.Add(creature);
    }

    public void RemoveCreatrure(GameObject creature)
    {
        creatures.Remove(creature);
    }

    public void AddSpawnPoint(GameObject gameObject)
    {
        spawnPoints.Add(gameObject);
    }

    public void Attack(Combatant cmb)
    {
        if (cmb == combatant)
        {
            PlayerAttack(cmb);
        }
        else
        {
            EnemyAttack(cmb);
        }
    }

    void PlayerAttack(Combatant ch)
    {
        foreach (GameObject creature in creatures)
        {
            Combatant cmb = creature.GetComponentInChildren<Combatant>();
            Vector3 dist = (creature.transform.position - ch.transform.position);
            dist.y = 0.0f;

            float angle = Vector3.Angle(transform.Find("CharacterContainer").transform.forward, dist.normalized);

            if (cmb && (dist.sqrMagnitude < (ch.attackDistance * ch.attackDistance)) && (angle <= ch.attackAngle * 0.5f))
            {
                if (ch.DoDamage(cmb))
                {
                    GetComponent<Character>().AddScore(1);
                }
            }
        }
    }

    void EnemyAttack(Combatant cr)
    {
        Vector3 dist = (gameObject.transform.position - cr.transform.position);
        dist.y = 0.0f;

        float angle = Vector3.Angle(cr.transform.forward, dist.normalized);

        if (combatant && (dist.sqrMagnitude < (cr.attackDistance * cr.attackDistance)) && (angle <= cr.attackAngle * 0.5f))
        {
            cr.DoDamage(combatant);
        }
    }
}
