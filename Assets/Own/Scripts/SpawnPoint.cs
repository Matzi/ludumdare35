﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
    public bool IsActive = true;

    float time = 0.0f;

	// Use this for initialization
	void Start () {
        FindObjectOfType<CreatureManager>().AddSpawnPoint(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	    if (time > 0.0f)
        {
            time -= Time.deltaTime;
            if (time <= 0.0f)
            {
                IsActive = true;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        IsActive = false;
        time = 5.0f;
    }

    void OnTriggerLeave(Collider other)
    {
        IsActive = true;
    }
}
