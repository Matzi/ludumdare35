﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    public int WeaponState = 0;//unarmed, 1H, 2H, bow, dual, pistol, rifle, spear, (sword and shield)
    public int NumPowerUp = 1;
    public float PowerUpDuration = 15.0f;
    public float HealingFactor = 10.0f;
    public Vector3 movementTargetPosition;

    int[] weapons = { 0, 1, 2, 4, 7, 8 };
    float[] attackDistances = { 2.3f, 2.8f, 3.4f, 2.4f, 4.0f, 2.8f };
    float[] attackTimes = { 0.6f, 0.5f, 0.8f, 0.7f, 1.6f, 1.3f };
    float[] attackHits = { 0.4f, 0.35f, 0.6f, 0.5f, 1.1f, 1.0f };
    float[] attackAngles = { 110, 110, 160, 110, 220, 110 };
    float[] attackDamages = { 55, 65, 80, 100, 65, 65 };
    float[] attackArmors = { 4, 8, 8, 8, 8, 14 };

    Animator amazonAnimator;
    Animator izzyAnimator;
    CharacterController ctrl;
    CreatureManager manager;
    Combatant combatant;
    Transform amazon;
    Transform izzy;
    GUIStyle labelStyle;

    int Score = 0;

    Quaternion targetRotation;
    int powerUp = 0;
    int totalSouls = 0;
    float rotateSpeed = 10.0f;
    float runSpeed = 7.0f;
    float walkSpeed = 1.9f;
    float superTimer = 0.0f;
    bool isInCombat = true;

    bool isAmazon = false;


    // Use this for initialization
    void Start()
    {
        amazon = transform.Find("CharacterContainer/Amazon");
        izzy = transform.Find("CharacterContainer/Izzy");
        amazonAnimator = amazon.GetComponentInChildren<Animator>();
        izzyAnimator = izzy.GetComponentInChildren<Animator>();
        manager = GetComponentInChildren<CreatureManager>();
        ctrl = GetComponentInChildren<CharacterController>();
        combatant = GetComponentInChildren<Combatant>();
        movementTargetPosition = transform.position;
        amazonAnimator.SetBool("NonCombat", !isInCombat);
        izzyAnimator.SetBool("NonCombat", !isInCombat);
        targetRotation = transform.Find("CharacterContainer").rotation;

        SetAmazon(isAmazon);
        ChangeWeapon(0);

        labelStyle = new GUIStyle();
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.fontSize = 24;
        labelStyle.normal.textColor = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        if (combatant.IsDead)
        {
            izzyAnimator.SetInteger("Death", Random.Range(1, 4));
            amazonAnimator.SetInteger("Death", Random.Range(1, 4));
            GetComponentInChildren<CharacterController>().enabled = false;
            return;
        }

        if (superTimer > 0.0f)
        {
            superTimer -= Time.deltaTime;
            if (superTimer <= 0.0f)
            {
                SetAmazon(false);
                superTimer = 0.0f;
            }
        }

        if (combatant.wasDamaged)
        {
            //izzyAnimator.SetTrigger("Pain");
            //amazonAnimator.SetTrigger("Pain");
            combatant.wasDamaged = false;
        }

        float horizontal = Mathf.Clamp(Input.GetAxis("Horizontal"), -1, 1);
        float vertical = Mathf.Clamp(Input.GetAxis("Vertical"), -1, 1);

        movementTargetPosition += Vector3.forward * horizontal;
        movementTargetPosition += Vector3.left * vertical;
        if (movementTargetPosition.sqrMagnitude > 0.5f) { movementTargetPosition.Normalize(); }

        if (combatant.CanMove())
        {
            if (Input.GetAxis("Attack") > 0.0f)
            {
                amazonAnimator.SetTrigger("Use");
                amazonAnimator.SetBool("Idling", true);
                izzyAnimator.SetTrigger("Use");
                izzyAnimator.SetBool("Idling", true);
                isInCombat = true;
                amazonAnimator.SetBool("NonCombat", !isInCombat);
                izzyAnimator.SetBool("NonCombat", !isInCombat);

                combatant.LaunchAttack();
            }
        }
        /*
        switch (Input.inputString)
        {
            case "0":
                WeaponState = 0;//unarmed
                break;
            case "1":
                WeaponState = 1;//1H one handed weapon
                break;
            case "2":
                WeaponState = 2;//2H two handed weapon(longsword or heavy axe)
                break;
            case "3":
                WeaponState = 3;//bow
                break;
            case "4":
                WeaponState = 4;//dual weild(short swords, light axes)
                break;
            case "5":
                WeaponState = 5;//pistol
                break;
            case "6":
                WeaponState = 6;//rifle
                break;
            case "7":
                WeaponState = 7;//spear
                break;
            case "8":
                WeaponState = 8;//Sword and Shield
                break;


            case "p":
                amazonAnimator.SetTrigger("Pain");
                izzyAnimator.SetTrigger("Pain");
                break;
            case "n":
                isInCombat = !isInCombat;
                amazonAnimator.SetBool("NonCombat", !isInCombat);
                izzyAnimator.SetBool("NonCombat", !isInCombat);
                break;
            case "m":
                SetAmazon(!isAmazon);
                break;
            default:
                break;
        }*/
        
        if (amazonAnimator != null)
        {
            amazonAnimator.SetInteger("WeaponState", WeaponState);
        }

        Transform trans = transform.Find("CharacterContainer");
        Quaternion tempRot = trans.rotation;

        if (movementTargetPosition.sqrMagnitude > 0.5f)
        {
            Vector3 turn = movementTargetPosition;
            turn.x = 0;

            if (turn.sqrMagnitude > 0.1f)
            {
                trans.LookAt(trans.position + turn);
                targetRotation = trans.rotation;
            }

            amazonAnimator.SetBool("Idling", false);
            izzyAnimator.SetBool("Idling", false);

            if (combatant.CanMove())
            {
                ctrl.Move(movementTargetPosition * (Time.deltaTime * (isInCombat ? runSpeed : walkSpeed)));
            }
        }
        else
        {
            amazonAnimator.SetBool("Idling", true);
            izzyAnimator.SetBool("Idling", true);
        }

        trans.rotation = Quaternion.Slerp(tempRot, targetRotation, Time.deltaTime * rotateSpeed);
        ctrl.Move(Vector3.down * (Time.deltaTime * 10.0f));

        movementTargetPosition = Vector3.zero;
    }

    void SetAmazon(bool setAmazon)
    {
        if (isAmazon != setAmazon)
        {
            isAmazon = setAmazon;

            if (isAmazon)
            {
                ChangeWeapon(Random.Range(1, weapons.Length));

                izzy.localPosition = Vector3.one * -100.0f;
                amazon.localPosition = Vector3.zero;
            }
            else
            {
                ChangeWeapon(0);
                izzy.localPosition = Vector3.zero;
                amazon.localPosition = Vector3.one * -100.0f;
            }

            GetComponentInChildren<ParticleSystem>().Play();
        }

        if (isAmazon)
        {
            superTimer += PowerUpDuration;
        }
    }

    public void ChangeWeapon(int weapon)
    {
        WeaponState = weapons[weapon];
        combatant.attackDistance = attackDistances[weapon];
        combatant.attackTime = attackTimes[weapon];
        combatant.attackAngle = attackAngles[weapon];
        combatant.hitTime = attackHits[weapon];
        combatant.Armor = attackArmors[weapon];
        combatant.Damage = attackDamages[weapon];
    }

    public void PowerUp()
    {
        ++totalSouls;
        ++powerUp;
        combatant.Heal(HealingFactor);
        combatant.Improve(1, 0);

        if (powerUp >= NumPowerUp)
        {
            powerUp -= NumPowerUp;
            ++NumPowerUp;
            SetAmazon(true);
        }
    }
    
    public int GetScore()
    {
        return Score;
    }

    public void AddScore(int i)
    {
        Score += i;
    }

    void OnGUI()
    {
        string hpString = "Health: " + (int)combatant.Health + " / " + (int)combatant.MaxHealth;
        string soulString = "Souls: " + powerUp + " / " + NumPowerUp;
        string scoreString = "Score: " + Score;
        GUI.Label(new Rect(10, 5, 1000, 40), hpString, labelStyle);
        GUI.Label(new Rect(280, 5, 1000, 40), soulString, labelStyle);
        GUI.Label(new Rect(480, 5, 1000, 40), scoreString, labelStyle);
    }
}
