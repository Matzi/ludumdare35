============================================ 
== Amelie the Amazing Amazon
============================================

Created as an entry for Ludum Dare 35 on the 72 hour game jam. 
http://ludumdare.com/compo/ludum-dare-35/?action=preview&uid=56958

This game is about a young girl, Amelie, who on one day notices that she has the power to turn into a fierce and powerful amazon. 
Not just an amazon, an Amazing Amazon! Her bad luck is that this day is when forces of evil attack earth, and she is the only one to defend it. 
(At least the weather is nice and sunny. Who wants apocalypse on a gloomy day?!) 
Collect the souls of the damned, to tap your limitless power, and fight for your life! 

Programming by Mátyás Bula

Contact: matzigon AT gmail DOT com

Music and art is free to use resource from unity store.
=============================================
This work is licensed under CC 3.0 http://creativecommons.org/licenses/by/3.0/

